<?php

namespace App\Models\Auditlog;

use Illuminate\Database\Eloquent\Model;
use App\Models\Departments\Departments;
use App\Models\User\User;

class Auditlog extends Model
{
    public $fillable = [
        'user_id','type','action','Code','department','ip'
    ];

    protected $appends = ['codename'];

    public function getCodenameAttribute()
    {
        if($this->Code == 1){
            return "Creating";
        }elseif($this->Code == 2){
            return "Editing";
        }elseif($this->Code == 3){
            return "Deleting";
        }elseif($this->Code == 4){
            return "Logged in";
        }elseif($this->Code == 5){
            return "Logout";
        }else{
            return "Unknown";
        }
    }

    public function user () {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function centerdesc () {
        return $this->belongsTo(Departments::class, 'department');
    }

    public function scopeAuditlog($query)
    {
        $center = request()->user()->department;
        return $query->with(["user", "centerdesc"])->where('department', $center)->orderBy("id", "DESC")->paginate(27);
    }
}
