<?php

namespace App\Models\Priority;

use Illuminate\Database\Eloquent\Model;
use App\Models\User\User;

class Priority extends Model
{
    public $fillable = ['name','description','user_id'];

    public function user () {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function scopePrioritieslist($query)
    {
        return $query->get();
    }

    public function scopePriorities($query)
    {
        return $query->with(["user"])->latest()->paginate(15);
    }
}
