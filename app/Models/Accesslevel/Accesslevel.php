<?php

namespace App\Models\Accesslevel;

use Illuminate\Database\Eloquent\Model;
use App\Models\User\User;

class Accesslevel extends Model
{
    public $fillable = ['name','description','user_id'];

    public function user () {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function scopeAccesslevellist($query)
    {
        return $query->get();
    }

    public function scopeAccesslevel($query)
    {
        return $query->with(["user"])->latest()->paginate(15);
    }
}
