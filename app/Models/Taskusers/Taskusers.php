<?php

namespace App\Models\Taskusers;

use Illuminate\Database\Eloquent\Model;
use App\Models\Tasks\Tasks;
use App\Models\Departments\Departments;
use App\Models\User\User;

class Taskusers extends Model
{
    public $fillable = ['tasks_id','user_id','department_id','comment'];

    protected $appends = ['username','departmentname'];

    public function getUsernameAttribute()
    {
        return User::where('id',$this->user_id)->pluck('firstname')->first();
    }

    public function getDepartmentnameAttribute()
    {
        return Departments::where('id',$this->department_id)->pluck('name')->first();
    }

    public function user () {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function task () {
        return $this->belongsTo(Tasks::class, 'tasks_id');
    }

    public function department () {
        return $this->belongsTo(Departments::class, 'department_id');
    }
}
