<?php

namespace App\Models\Progressstatus;

use Illuminate\Database\Eloquent\Model;
use App\Models\User\User;

class Progressstatus extends Model
{
    public $fillable = ['name','description','user_id'];

    public function user () {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function scopeProggresslist($query)
    {
        return $query->get();
    }

    public function scopeProggress($query)
    {
        return $query->with(["user"])->latest()->paginate(15);
    }
}
