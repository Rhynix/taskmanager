<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Models\Departments\Departments;
use Illuminate\Support\Str;
use App\Models\Traits\AvatarUrlLink;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use AvatarUrlLink;
    protected $fillable = [
        'firstname','lastname','email','password','department','roleid','accountstatus','screenlock','online','avatar','phone','theme','user_id','uuid'
    ];

    // public $timestamps = true;

    public static function boot()
    {
        parent::boot();
        static::creating(function ($user) {
            $user->uuid = Str::uuid();
        });
    }

    protected $appends = ['full_name','avatarlink','abraviation','state','role','departmentname','themename', 'lable', 'value'];

    public function routeNotificationForSlack($notification)
    {
        return 'https://hooks.slack.com/services/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';
    }

    public function getAbraviationAttribute()
    {
        return substr($this->firstname, 0, 1).'.'.substr($this->lastname, 0, 1);
    }

    public function getLableAttribute()
    {
        return $this->id;
    }

    public function getValueAttribute()
    {
        return $this->firstname.' '.$this->lastname;
    }

    public function user () {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getStateAttribute()
    {
        if ($this->accountstatus == 1) {
            return "Active";
        }else{
            return "Inactive";
        }
    }

    public function getThemenameAttribute()
    {
        if ($this->theme == 1) {
            return "Light";
        }else{
            return "Dark";
        }
    }

    public function getDepartmentnameAttribute()
    {
        return Departments::where('code',$this->department)->pluck('name')->first();
    }

    public function getRoleAttribute()
    {
        if ($this->roleid == 1) {
            return "Employee";
        }elseif ($this->roleid == 2) {
            return "Member";
        }elseif ($this->roleid == 3){
            return "Manager";
        }else{}
    }
    
    public function getFullNameAttribute()
    {
        return $this->firstname." ".$this->lastname;
    }

    protected $hidden = [
        'password'
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function scopeUserslist($query)
    {
        return $query->get();
    }

    public function centerdesc () {
        return $this->belongsTo(Departments::class, 'department');
    }
}
