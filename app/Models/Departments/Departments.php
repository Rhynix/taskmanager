<?php

namespace App\Models\Departments;

use Illuminate\Database\Eloquent\Model;
use App\Models\User\User;

class Departments extends Model
{
    public $fillable = [
        'code','name','email','user_id'
    ];
    
    protected $primaryKey = 'code';
    public $incrementing = false;

    public static function getcentername($code)
    {
        return static::where('code', $code)->first();
    }

    public function user () {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function scopeCenters($query)
    {
        return $query->with(["user"])->get();
    }
}
