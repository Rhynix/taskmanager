<?php

namespace App\Models\Taskcomments;

use Illuminate\Database\Eloquent\Model;
use App\Models\User\User;
use App\Models\Tasks\Tasks;

class Taskcomments extends Model
{
    public $fillable = ['tasks_id','user_id','comment'];

    public function user () {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function task () {
        return $this->belongsTo(Tasks::class, 'tasks_id');
    }
}
