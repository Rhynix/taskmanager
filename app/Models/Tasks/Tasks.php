<?php

namespace App\Models\Tasks;

use Illuminate\Database\Eloquent\Model;
use App\Models\User\User;
use App\Models\Accesslevel\Accesslevel;
use App\Models\Categories\Categories;
use App\Models\Priority\Priority;
use App\Models\Departments\Departments;
use App\Models\Progressstatus\Progressstatus;
use App\Models\Taksusers\Taskusers;
use App\Models\Taskcomments\Taskcomments;
use Illuminate\Support\Str;
use App\Models\Taskdatesadjustments\Taskdatesadjustments;

class Tasks extends Model
{
    public $fillable = ['user_id','task_uid','due_date','progress_status','title','description','access_level','category','priority','department','assigened_user'];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($task) {
            $task->task_uid = Str::uuid();
        });
    }

    public function assignees () {
        return $this->hasMany(Taskusers::class);
    }
    
    public function dateadjustments () {
        return $this->hasMany(Taskdatesadjustments::class);
    }

    public function comments () {
        return $this->hasMany(Taskcomments::class);
    }

    public function user () {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function accessdesc () {
        return $this->belongsTo(Accesslevel::class, 'access_level');
    }

    public function categorydesc () {
        return $this->belongsTo(Categories::class, 'category');
    }

    public function prioritydesc () {
        return $this->belongsTo(Priority::class, 'priority');
    }

    public function departmentdesc () {
        return $this->belongsTo(Departments::class, 'department');
    }

    public function assigened () {
        return $this->belongsTo(User::class, 'assigened_user');
    }

    public function progress () {
        return $this->belongsTo(Progressstatus::class, 'progress_status');
    }

    public function scopeTasks($query)
    {
        return $query->with(["assignees","dateadjustments","comments","accessdesc","categorydesc","prioritydesc","departmentdesc","assigened","progress","user"])->latest()->paginate(15);
    }

    public function scopeMytasks($query)
    {
        $id = request()->user()->id;
        return $query->where('assigened_user',$id)->with(["assignees","dateadjustments","comments","accessdesc","categorydesc","prioritydesc","departmentdesc","assigened","progress","user"])->latest()->paginate(15);
    }
}
