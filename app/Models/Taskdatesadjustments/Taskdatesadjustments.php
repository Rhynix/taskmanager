<?php

namespace App\Models\Taskdatesadjustments;

use Illuminate\Database\Eloquent\Model;
use App\Models\User\User;
use App\Models\Departments\Departments;
use App\Models\Tasks\Tasks;

class Taskdatesadjustments extends Model
{
    public $fillable = ['tasks_id','date_to','date_from','user_id','department_id','reason'];

    public function tasks () {
        return $this->belongsTo(Tasks::class, 'tasks_id');
    }

    public function user () {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function department () {
        return $this->belongsTo(Departments::class, 'department_id');
    }
}
