<?php

namespace App\Models\Categories;

use Illuminate\Database\Eloquent\Model;
use App\Models\User\User;

class Categories extends Model
{
    public $fillable = ['name','description','user_id'];

    public function user () {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function scopeCategorylist($query)
    {
        return $query->get();
    }

    public function scopeCategory($query)
    {
        return $query->with(["user"])->latest()->paginate(15);
    }
}
