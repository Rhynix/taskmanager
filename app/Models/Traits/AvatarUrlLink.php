<?php

namespace App\Models\Traits;

trait AvatarUrlLink
{
    public function getAvatarlinkAttribute()
    {
        // return secure_url('storage').'/';
        return asset('storage').'/';
    }
}
