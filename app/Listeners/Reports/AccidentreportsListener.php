<?php

namespace App\Listeners\Reports;

use App\Events\Reports\PoolreportsEvent;
use App\Models\Reporttables\Accidentreports\Accidentreports;

class AccidentreportsListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PoolreportsEvent  $event
     * @return void
     */
    public function handle(PoolreportsEvent $event)
    {
        $updater = Accidentreports::where('apislug',$event->slug);
        $updater->update([
            'user_id'   => request()->user()->id,
            'costCenter'=> request()->user()->costCenter,
        ]);
    }
}
