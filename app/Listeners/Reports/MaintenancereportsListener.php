<?php

namespace App\Listeners\Reports;

use App\Events\Reports\PoolreportsEvent;
use App\Models\Reporttables\Maintenancereport\Maintenancereport;

class MaintenancereportsListener
{
    // protected $cart;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PoolreportsEvent  $event
     * @return void
     */
    public function handle(PoolreportsEvent $event)
    {
        $updater = Maintenancereport::where('apislug',$event->slug);
        $updater->update([
            'user_id'   => request()->user()->id,
            'costCenter'=> request()->user()->costCenter,
        ]);
    }
}
