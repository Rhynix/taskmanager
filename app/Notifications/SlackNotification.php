<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;
use App\Models\User\User;

class SlackNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack'];
    }

    public function toSlack($user)
    {
        // dd($user->user->firstname);
        // $url = url('/exceptions/'.request()->firstname);
        return (new SlackMessage)
                ->from('Feetwise User Module')
                ->image('http://fleetwise.co/img/logo.svg')
                ->success()
                ->content("{$user->firstname} {$user->lastname}'s details updated.")
                ->attachment(function ($attachment) use ($user) {
                    $attachment->title(":tractor: This user id is: {$user->id}.")
                                ->fields([
                                    'Email' => $user->email,
                                    'Cost Center' => $user->centername,
                                    'User Role' => $user->role,
                                    'Account status' => $user->state,
                                    'Phone number' => $user->phone,
                                    'Theme' => $user->themename,
                                    'Updated by' => $user->user->firstname.' '.$user->user->lastname,
                                    'Updated at' => $user->updated_at,
                                ]);
                            //    ->content('File [background.jpg] was not found.'.request()->firstname);
                });
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    // public function toArray($notifiable)
    // {
    //     return [
    //         //
    //     ];
    // }
}
