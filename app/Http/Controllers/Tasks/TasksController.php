<?php

namespace App\Http\Controllers\Tasks;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tasks\Tasks;
use App\Helper\Newlog;
use App\Http\Requests\Tasks\TasksRequest;
use App\Http\Requests\Tasks\TasksUpdateRequest;
use Mail;
use App\Mail\TaskAssignedMail;
use App\Models\User\User;
use App\Models\Taskcomments\Taskcomments;
use App\Http\Requests\TaskComment\TaskcommentsRequest;
use App\Models\Taskusers\Taskusers;
use App\Http\Requests\Taskusers\TaskusersRequest;
use App\Models\Departments\Departments;

class TasksController extends Controller
{
    public function index(Request $request)
    {
        $takes = Tasks::tasks();
        return response($takes);
    }

    public function newcomment(TaskcommentsRequest $request)
    {
        $newtask = Taskcomments::create([
            'user_id'       => request()->user()->id,
            'tasks_id'      => $request->id,
            'comment'       => $request->newComment,
        ]);

        Newlog::addnewlog("User Event", "Created a new task: $request->newComment", 1);
        return response()->json(['data' => $newtask], 201);
    }

    public function newtaskuser(TaskusersRequest $request)
    {
        $department = ($request->datatype == 1) ? $request->userDepart : null;
        $user = ($request->datatype == 2) ? $request->userDepart : null;
        $newtask = Taskusers::create([
            'user_id'       => $user,
            'tasks_id'      => $request->id,
            'department_id' => $department,
            'comment'       => $request->userComments,
        ]);

        Newlog::addnewlog("User Event", "Added a user to task: $request->id", 1);
        return response()->json(['data' => $newtask], 201);
    }

    public function mytasks(Request $request)
    {
        $tasks = Tasks::mytasks();
        return response($tasks);
    }

    public function store(TasksRequest $request)
    {
        $retVal = (empty($request->category)) ? 1 : $request->category;
        $newtask = Tasks::create([
            'user_id'           => request()->user()->id,
            'due_date'          => $request->dueDate,
            'progress_status'   => $request->progressStatus,
            'title'             => $request->title,
            'description'       => $request->description,
            'access_level'      => $request->accessLevel,
            'category'          => $retVal,
            'priority'          => $request->priority,
            'department'        => $request->department,
            'assigened_user'    => $request->user,
        ]);

        if (!empty($request->user)) {
            $user = User::findOrFail(intval($request->user));
        }else{
            $user = Departments::findOrFail(intval($request->department));
        }
        
        $pathToFile = "/images/logo.png";
        $today = date("F j Y, g:i a");
        $appurl = config('app.url');
        try {
            Mail::to($user->email)->later(10, new TaskAssignedMail($user, $newtask, $pathToFile, $today, $appurl));
        } catch (Swift_TransportException $e) {
            Log.info("Mail not sent, reasons: ".$e->getMessage());
        }
        
        $task = Tasks::with(["assignees","comments","accessdesc","categorydesc","prioritydesc","departmentdesc","assigened","progress","user"])->findOrFail(intval($newtask->id));

        Newlog::addnewlog("User Event", "Created a new task: $request->description", 1);
        return response()->json(['data' => $task], 201);
    }

    public function update(TasksUpdateRequest $request, $id)
    {
        $task = Tasks::findOrFail(intval($id));

        $task->due_date           = $request->dueDate;
        $task->progress_status    = $request->progressStatus;
        $task->title              = $request->title;
        $task->description        = $request->description;
        $task->access_level       = $request->accessLevel;
        $task->category           = $request->category;
        $task->priority           = $request->priority;
        $task->department         = $request->department;
        $task->assigened_user     = $request->user;
        $task->save();
        Newlog::addnewlog("User Event", "Updated task: $request->description", 2);
        return response()->json(['data' => $task], 200);
    }

    public function search(Request $request)
    {
        $search = $request->search;
        if ($request->search != null) {
            $proggress = Tasks::with(["user"])->where('description', 'LIKE', '%' . $search . '%')->orderBy("id", "ASC")->paginate(15);
            return response($proggress);
        }
    }
}
