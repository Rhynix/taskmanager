<?php

namespace App\Http\Controllers\Categories;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Categories\Categories;
use App\Http\Resources\Collectionresource\CollectionResource;
use App\Http\Requests\Categories\CategoriesUpdateRequest;
use App\Http\Requests\Categories\CategoriesRequest;
use App\Helper\Newlog;

class CategoriesController extends Controller
{
    public function index(Request $request)
    {
        $makes = Categories::category();
        return response($makes);
    }

    public function categorieslist(Request $request)
    {
        $Progress = Categories::categorylist();
        return CollectionResource::collection(
            $Progress
        );
    }

    public function store(CategoriesRequest $request)
    {
        $progress = Categories::create([
            'name' => $request->name,
            'description' => $request->description,
            'user_id' => request()->user()->id,
        ]);

        Newlog::addnewlog("User Event", "Created a new category: $request->description", 1);
        return response()->json(['data' => $progress], 201);
    }

    public function update(CategoriesUpdateRequest $request, $id)
    {
        $status = Categories::findOrFail(intval($id));

        $status->name = $request->name;
        $status->description = $request->description;
        $status->save();
        Newlog::addnewlog("User Event", "Updated category: $request->description", 2);
        return response()->json(['data' => $status], 200);
    }

    public function search(Request $request)
    {
        $search = $request->search;
        if ($request->search != null) {
            $proggress = Categories::with(["user"])->where('description', 'LIKE', '%' . $search . '%')->orderBy("id", "ASC")->paginate(15);
            return response($proggress);
        }
    }
}
