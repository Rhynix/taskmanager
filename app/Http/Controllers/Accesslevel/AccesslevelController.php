<?php

namespace App\Http\Controllers\Accesslevel;

use App\Http\Resources\Collectionresource\CollectionResource;
use App\Http\Requests\Accesslevel\AccesslevelUpdateRequest;
use App\Http\Requests\Accesslevel\AccesslevelRequest;
use App\Models\Accesslevel\Accesslevel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helper\Newlog;

class AccesslevelController extends Controller
{
    public function index(Request $request)
    {
        $makes = Accesslevel::accesslevel();
        return response($makes);
    }

    public function accesslevellist(Request $request)
    {
        $Progress = Accesslevel::accesslevellist();
        return CollectionResource::collection(
            $Progress
        );
    }

    public function store(AccesslevelRequest $request)
    {
        $progress = Accesslevel::create([
            'name' => $request->name,
            'description' => $request->description,
            'user_id' => request()->user()->id,
        ]);

        Newlog::addnewlog("User Event", "Created a new category: $request->description", 1);
        return response()->json(['data' => $progress], 201);
    }

    public function update(AccesslevelUpdateRequest $request, $id)
    {
        $status = Accesslevel::findOrFail(intval($id));

        $status->name = $request->name;
        $status->description = $request->description;
        $status->save();
        Newlog::addnewlog("User Event", "Updated category: $request->description", 2);
        return response()->json(['data' => $status], 200);
    }

    public function search(Request $request)
    {
        $search = $request->search;
        if ($request->search != null) {
            $proggress = Accesslevel::with(["user"])->where('description', 'LIKE', '%' . $search . '%')->orderBy("id", "ASC")->paginate(15);
            return response($proggress);
        }
    }
}
