<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\Auth\ResetPasswordRequest;
use App\Http\Controllers\Controller;
use App\Mail\PasswordResetMail;
use App\Helper\Utilities;
use App\Models\User\User;
use Mail;

class ForgotCredentialsController extends Controller
{
    private $subject = "Credentials Reset";

    public function resetPassword(ResetPasswordRequest $request) {
        $newPassword = Utilities::generatePassword();
        $user = User::where('email',$request->email)->first();
        $user->password = bcrypt($newPassword);
        $user->save();
        $this->updatedpassword($user, $newPassword);
        return response()->json([
            'data' => 'Your new password has been sent to the email provided'
        ], 200);
    }

    public function updatedpassword ($user, $password) {
        $password = empty($password) ? config('app.adminpass') : $password;
        $pathToFile = "/images/logo.png";
        $today = date("F j Y, g:i a");
        $appurl = config('app.url');
        try {
            Mail::to($user->email)->later(10, new PasswordResetMail($user, $password, $pathToFile, $today, $appurl));
        } catch (Swift_TransportException $e) {
            Log.info("Mail not sent, reasons: ".$e->getMessage());
        }
    }
}
