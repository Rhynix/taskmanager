<?php

namespace App\Http\Controllers\Taskdatesadjustments;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Taskdatesadjustments\Taskdatesadjustments;
use App\Http\Requests\Taskdatesadjustments\TaskdatesadjustmentsRequest;
use App\Helper\Newlog;

class TaskdatesadjustmentsController extends Controller
{
    public function store(TaskdatesadjustmentsRequest $request)
    {
        $newtask = Taskdatesadjustments::create([
            'user_id'       => request()->user()->id,
            'tasks_id'      => $request->id,
            'date_to'       => $request->date,
            'date_from'     => $request->dateFrom,
            'reason'        => $request->reason,
        ]);

        Newlog::addnewlog("User Event", "Created a new task: $request->description", 1);
        return response()->json(['data' => $newtask], 201);
    }
}
