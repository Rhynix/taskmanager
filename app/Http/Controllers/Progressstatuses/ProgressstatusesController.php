<?php

namespace App\Http\Controllers\Progressstatuses;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Progressstatus\Progressstatus;
use App\Http\Resources\Collectionresource\CollectionResource;
use App\Http\Requests\Progressstatus\ProgressstatusRequest;
use App\Helper\Newlog;
use App\Http\Requests\Progressstatus\ProgressstatusUpdateRequest;

class ProgressstatusesController extends Controller
{
    public function index(Request $request)
    {
        $makes = Progressstatus::proggress();
        return response($makes);
    }

    public function proggresslist(Request $request)
    {
        $Progress = Progressstatus::proggresslist();
        return CollectionResource::collection(
            $Progress
        );
    }

    public function store(ProgressstatusRequest $request)
    {
        $progress = Progressstatus::create([
            'name' => $request->name,
            'description' => $request->description,
            'user_id' => request()->user()->id,
        ]);

        Newlog::addnewlog("User Event", "Created a new progress status: $request->description", 1);
        return response()->json(['data' => $progress], 201);
    }

    public function update(ProgressstatusUpdateRequest $request, $id)
    {
        $status = Progressstatus::findOrFail(intval($id));

        $status->name = $request->name;
        $status->description = $request->description;
        $status->save();
        Newlog::addnewlog("User Event", "Updated progress status: $request->description", 2);
        return response()->json(['data' => $status], 200);
    }

    public function search(Request $request)
    {
        $search = $request->search;
        if ($request->search != null) {
            $proggress = Progressstatus::with(["user"])->where('description', 'LIKE', '%' . $search . '%')->orderBy("id", "ASC")->paginate(15);
            return response($proggress);
        }
    }
}
