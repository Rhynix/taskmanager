<?php

namespace App\Http\Controllers\Priorities;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Priority\Priority;
use App\Http\Requests\Priority\PriorityRequest;
use App\Http\Requests\Priority\PriorityUpdateRequest;
use App\Helper\Newlog;
use App\Http\Resources\Collectionresource\CollectionResource;

class PrioritiesController extends Controller
{
    public function index(Request $request)
    {
        $makes = Priority::priorities();
        return response($makes);
    }

    public function prioritieslist(Request $request)
    {
        $Progress = Priority::prioritieslist();
        return CollectionResource::collection(
            $Progress
        );
    }

    public function store(PriorityRequest $request)
    {
        $progress = Priority::create([
            'name' => $request->name,
            'description' => $request->description,
            'user_id' => request()->user()->id,
        ]);

        Newlog::addnewlog("User Event", "Created a new progress status: $request->description", 1);
        return response()->json(['data' => $progress], 201);
    }

    public function update(PriorityUpdateRequest $request, $id)
    {
        $status = Priority::findOrFail(intval($id));

        $status->name = $request->name;
        $status->description = $request->description;
        $status->save();
        Newlog::addnewlog("User Event", "Updated progress status: $request->description", 2);
        return response()->json(['data' => $status], 200);
    }

    public function search(Request $request)
    {
        $search = $request->search;
        if ($request->search != null) {
            $proggress = Priority::with(["user"])->where('description', 'LIKE', '%' . $search . '%')->orderBy("id", "ASC")->paginate(15);
            return response($proggress);
        }
    }
}
