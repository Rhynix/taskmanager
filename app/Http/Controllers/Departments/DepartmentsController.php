<?php

namespace App\Http\Controllers\Departments;

use App\Models\Departments\Departments;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Costcenter\CostcenterUpdateRequest;
use App\Http\Requests\Costcenter\CotcenterFormRequest;
use App\Helper\Newlog;
use App\Http\Resources\DepartmentResource;

class DepartmentsController extends Controller
{
    public function index(Request $request)
    {
        $department = Departments::centers();
        return response()->json([
            'data' => $department,
        ], 200);
    }

    public function selectlist(Request $request)
    {
        $department = Departments::centers();
        return DepartmentResource::collection(
            $department
        );
    }

    public function store(CotcenterFormRequest $request)
    {
        $center = Departments::create([
            'code'      => $request->companyCode,
            'name'      => $request->company,
            'email'     => $request->email,
            'user_id'   => request()->user()->id,
        ]);

        Newlog::addnewlog("User Event", "Added new cost center: $request->company with code: $request->companyCode", 1);
        return response()->json(['data' => $center ], 201);
    }

    public function update(CostcenterUpdateRequest $request, $code)
    {
        $center = Departments::findOrFail(intval($code));
        $center->update([
            'name'   => $request->company,
            'email'  => $request->email,
        ]);
        Newlog::addnewlog("User Event", "Updated cost center: $request->company", 2);
        return response()->json(['data'=>$center], 200);
    }
}
