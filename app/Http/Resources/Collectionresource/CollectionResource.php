<?php

namespace App\Http\Resources\Collectionresource;

use Illuminate\Http\Resources\Json\JsonResource;

class CollectionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'lable' => $this->id,
            'value' => $this->name,
            'description' => $this->description,
        ];
    }
}
