<?php

namespace App\Http\Resources\Userdepartment;

use Illuminate\Http\Resources\Json\JsonResource;

class UserdepartmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'lable' => ($this->code) ? $this->code : $this->id,
            'value' => ($this->code) ? $this->name : trim($this->firstname.' '.$this->lastname),
            'type' => ($this->code) ? 1 : 2,
        ];
    }
}
