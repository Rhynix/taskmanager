<?php

namespace App\Http\Requests\Users;

use App\Http\Requests\FormRequest;

class ProfileUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phoneNumber' => 'required|min:12|phone_number',
            'firstName' => 'required|min:3',
            'lastName'  => 'required|min:3',
            'avatar'    => 'required',
            'avatar'      => 'required'
        ];
    }
    public function messages()
    {
        return [
            'phoneNumber.phone_number' => 'Must be in (254711111111) formart and must not include spaces or special characters',
        ];
    }
}