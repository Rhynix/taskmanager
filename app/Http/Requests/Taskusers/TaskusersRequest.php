<?php

namespace App\Http\Requests\Taskusers;

use Illuminate\Foundation\Http\FormRequest;

class TaskusersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'            => 'required|exists:tasks,id',
            'userDepart'    => 'required|integer',
            'datatype'      => 'required|integer',
            'userComments'  => 'required|max:200|min:3',
        ];
    }
}
