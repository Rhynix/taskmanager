<?php

namespace App\Http\Requests\Accesslevel;

use Illuminate\Foundation\Http\FormRequest;

class AccesslevelUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'            => 'required|numeric|exists:accesslevels',
            'name'          => 'required|max:20|min:3',
            'description'   => 'required|max:100|min:3',
        ];
    }
}
