<?php

namespace App\Http\Requests\Tasks;

use Illuminate\Foundation\Http\FormRequest;

class TasksUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'                => 'required|integer|exists:tasks',
            'title'             => 'required|max:30|min:3',
            'description'       => 'required|max:100|min:3',
            'accessLevel'       => 'required|integer|exists:accesslevels,id',
            'priority'          => 'required|integer|exists:priorities,id',
            // 'dueDate'           => 'required|date',
            // 'progressStatus'    => 'required|integer|exists:progressstatuses,id',
            // 'category'          => 'required|integer|exists:categories,id',
        ];
    }
}
