<?php

namespace App\Http\Requests\Taskdatesadjustments;

use Illuminate\Foundation\Http\FormRequest;

class TaskdatesadjustmentsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'        => 'required|exists:tasks,id',
            'date'      => 'required|date',
            'reason'      => 'required|max:100|min:3',
            'dateFrom'  => 'required|date',
        ];
    }
}
