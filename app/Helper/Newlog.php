<?php
namespace App\Helper;
use Carbon\Carbon;
use App\Models\Auditlog\Auditlog;

class Newlog
{
    public static function addnewlog($event, $action, $code)
    {
        Auditlog::create([
            'user_id'   => request()->user()->id,
            'type'      => $event,
            'action'    => $action,
            'Code'      => $code,
            'department'=> request()->user()->department,
            'ip'        => request()->ip()
        ]);
    }
}
