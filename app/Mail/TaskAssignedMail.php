<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TaskAssignedMail extends Mailable
{
    use Queueable, SerializesModels;
    public $user, $newtask, $pathToFile, $today, $appurl;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $newtask, $pathToFile, $today, $appurl)
    {
        $this->user = $user;
        $this->newtask = $newtask;
        $this->pathToFile = $pathToFile;
        $this->today = $today;
        $this->appurl = $appurl;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Task Manager New Password")->view('emails.newtask');
    }
}
