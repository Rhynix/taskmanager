<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Reports\PoolreportsEvent' => [
            'App\Listeners\Reports\PoolreportsListener',
            'App\Listeners\Reports\FuelreportsListener',
            'App\Listeners\Reports\MaintenancereportsListener',
            'App\Listeners\Reports\CostingreportsListener',
            'App\Listeners\Reports\VehiclereportsListener',
            'App\Listeners\Reports\InventoryreportsListener',
            'App\Listeners\Reports\AccidentreportsListener'
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
