import moment from 'moment'

export const setSelectedTask = (state, data) => {
	state.task = data
}


export const setVehicleJobcardCounter = (state, data) => {
	state.vehicleJobcardCounter = data
}

export const setServicetypesCount = (state, data) => {
	state.servicescount = data
}

export const setLeaveCount = (state, data) => {
	state.leavecount = data
}

export const changefuelmonth = (state, data) => {
	state.fuelmonth = moment(data)
}

export const changebookmonth = (state, data) => {
	state.bookingmonth = moment(data)
}

export const setMechanicsJob = (state, data) => {
	state.mechanicsjob = data
}

export const setBookingChart = (state, data) => {
	state.bookingChart = data
}



export const setFuelChart = (state, data) => {
	state.fuelchart = data
}