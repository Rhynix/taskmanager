import Vue from 'vue'
export const Tasks = Vue.component('tasks', require('./Tasks'))
export const Taskform = Vue.component('taskform', require('./Taskform'))
export const Mytasks = Vue.component('mytasks', require('./Mytasks'))
export const Taskrow = Vue.component('taskrow', require('./taskrow'))