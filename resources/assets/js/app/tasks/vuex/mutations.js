export const setTasks = (state, data) => {
	state.tasks = data
}

export const setMyTasks = (state, data) => {
	state.myTasks = data
}

export const setSelectCenter = (state, data) => {
	state.selectCenter = data
}

export const setCenterList = (state, data) => {
	state.centerlist = data
}