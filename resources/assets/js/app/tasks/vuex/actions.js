export const fetchTasks = ({ commit }) => {
    return axios.get('/api/tasks').then((response) => {
        commit('setTasks', response.data.data)
    })
}

export const myTasks = ({ commit }) => {
    return axios.get('/api/mytasks').then((response) => {
        commit('setMyTasks', response.data.data)
    })
}

export const selectedCenter = ({ commit }, payload) => {
    commit('setSelectCenter', payload)
}

export const updatTask = ({ commit }, { payload, context }) => {
    return axios.put('/api/task/' + payload.id + '/update', payload).then((response) => {
    }).catch((error) => {
        context.errors = error.response.data.errors
   })
}

export const createTask = ({commit }, { payload, context }) => {
    return axios.post('/api/task/create', payload).then((response) => {
        context.newdata = response.data.data
    }).catch((error) => {
        context.errors = error.response.data.errors
    })
}

export const addExtendedDate = ({commit }, { payload, context }) => {
    return axios.post('/api/taskdatesadjustments/create', payload).then((response) => {
        context.newdata = response.data.data
    }).catch((error) => {
        context.errors = error.response.data.errors
    })
}

export const addCommentStaff = ({commit }, { payload, context }) => {
    return axios.post('/api/newcomment/create', payload).then((response) => {
        context.newcommentdata = response.data.data
    }).catch((error) => {
        context.errors = error.response.data.errors
    })
}

export const addUserDepartmentComment = ({commit }, { payload, context }) => {
    return axios.post('/api/newtaskuser/create', payload).then((response) => {
        context.newuserdepartment = response.data.data
    }).catch((error) => {
        context.errors = error.response.data.errors
    })
}

export const fetchCenterList = ({ commit }) => {
    return axios.get('/api/centerlist').then((response) => {
        commit('setCenterList', response.data.data)
    })
}