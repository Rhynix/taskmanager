export const getTasks = (state) => {
    return state.tasks
}

export const getMyTasks = (state) => {
    return state.myTasks
}

export const getCenter = (state) => {
    return state.selectCenter
}

export const getCenterList = (state) => {
    return state.centerlist
}