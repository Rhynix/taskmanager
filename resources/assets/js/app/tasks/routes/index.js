import { Tasks, Mytasks } from '../components'

export default [
    {
        path: '/tasks',
        component: Tasks,
        name: 'tasks',
        meta: {
            needsAuth: true
        }
    },
    {
        path: '/my-tasks',
        component: Mytasks,
        name: 'my-tasks',
        meta: {
            needsAuth: true
        }
    }
]