import dashboard from './dashboard/routes'
import users from './users/routes'
import messages from './messages/routes'
import auth from './auth/routes'
import errors from './errors/routes'
import welcome from './welcome/routes'
import costcenters from './costcenters/routes'
import tasks from './tasks/routes'
import progressstatus from './progressstatus/routes'
import categories from './categories/routes'
import accesslevels from './accesslevels/routes'
import priorities from './priorities/routes'
import userdashboard from './userdashboard/routes'

export default 
[
    ...dashboard,
    ...users,
    ...messages,
    ...auth,
    ...welcome,
    ...costcenters,
    ...tasks,
    ...progressstatus,
    ...categories,
    ...accesslevels,
    ...priorities,
    ...userdashboard,
    ...errors
]