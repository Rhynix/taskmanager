export const getPriorities = (state) => {
    return state.priorities
}

export const getPriority = (state) => {
    return state.priority
}

export const getPrioritiesList = (state) => {
    return state.prioritieslist
}