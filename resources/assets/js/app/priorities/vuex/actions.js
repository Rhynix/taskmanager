export const fetchpriorities = ({ commit }) => {
    return axios.get('/api/priorities').then((response) => {
        commit('setPriorities', response.data.data)
    })
}

export const selectedpriority = ({ commit }, payload) => {
    commit('setselectedtPriority', payload)
}

export const updatPriority = ({ commit }, { payload, context }) => {
    return axios.put('/api/priority/' + payload.id + '/update', payload).then((response) => {
    }).catch((error) => {
        context.errors = error.response.data.errors
   })
}

export const createPriority = ({commit }, { payload, context }) => {
    return axios.post('/api/priority/create', payload).then((response) => {
        context.newdata = response.data.data
    }).catch((error) => {
        context.errors = error.response.data.errors
    })
}

export const fetchPrioritiesList = ({ commit }) => {
    return axios.get('/api/prioritieslist').then((response) => {
        commit('setPrioritiesList', response.data.data)
    })
}