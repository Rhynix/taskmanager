import { Priorities } from '../components'

export default [
    {
        path: '/priorities',
        component: Priorities,
        name: 'priorities',
        meta: {
            needsAuth: true
        }
    }
]