export const fetchaccesslevels = ({ commit }) => {
    return axios.get('/api/accesslevels').then((response) => {
        commit('setStatus', response.data.data)
    })
}

export const selectedaccesslevel = ({ commit }, payload) => {
    commit('setselectedtstatus', payload)
}

export const updatAccesslevel = ({ commit }, { payload, context }) => {
    return axios.put('/api/accesslevel/' + payload.id + '/update', payload).then((response) => {
    }).catch((error) => {
        context.errors = error.response.data.errors
   })
}

export const createAccesslevel = ({commit }, { payload, context }) => {
    return axios.post('/api/accesslevel/create', payload).then((response) => {
        context.newdata = response.data.data
    }).catch((error) => {
        context.errors = error.response.data.errors
    })
}

export const fetchAccesslevelList = ({ commit }) => {
    return axios.get('/api/accesslevelslist').then((response) => {
        commit('setStatusList', response.data.data)
    })
}