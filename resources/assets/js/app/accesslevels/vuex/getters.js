export const getStatus = (state) => {
    return state.status
}

export const getState = (state) => {
    return state.selectstate
}

export const getAccesslevelList = (state) => {
    return state.statuslist
}