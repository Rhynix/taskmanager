import { Accesslevels } from '../components'

export default [
    {
        path: '/accesslevels',
        component: Accesslevels,
        name: 'accesslevels',
        meta: {
            needsAuth: true
        }
    }
]