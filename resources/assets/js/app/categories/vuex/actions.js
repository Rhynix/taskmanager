export const fetchcategories = ({ commit }) => {
    return axios.get('/api/categories').then((response) => {
        commit('setStatus', response.data.data)
    })
}

export const selectedcategory = ({ commit }, payload) => {
    commit('setselectedtstatus', payload)
}

export const updatCategory = ({ commit }, { payload, context }) => {
    return axios.put('/api/category/' + payload.id + '/update', payload).then((response) => {
    }).catch((error) => {
        context.errors = error.response.data.errors
   })
}

export const createCategory = ({commit }, { payload, context }) => {
    return axios.post('/api/category/create', payload).then((response) => {
        context.newdata = response.data.data
    }).catch((error) => {
        context.errors = error.response.data.errors
    })
}

export const fetchCategoryList = ({ commit }) => {
    return axios.get('/api/categorieslist').then((response) => {
        commit('setStatusList', response.data.data)
    })
}