import { Progress } from '../components'

export default [
    {
        path: '/progress-status',
        component: Progress,
        name: 'progress-status',
        meta: {
            needsAuth: true
        }
    }
]