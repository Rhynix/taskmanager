export const fetchstatus = ({ commit }) => {
    return axios.get('/api/progressstatuses').then((response) => {
        commit('setStatus', response.data.data)
    })
}

export const selectedtstatus = ({ commit }, payload) => {
    commit('setselectedtstatus', payload)
}

export const updatStatus = ({ commit }, { payload, context }) => {
    return axios.put('/api/progressstatus/' + payload.id + '/update', payload).then((response) => {
    }).catch((error) => {
        context.errors = error.response.data.errors
   })
}

export const createstatus = ({commit }, { payload, context }) => {
    return axios.post('/api/progressstatus/create', payload).then((response) => {
        context.newdata = response.data.data
    }).catch((error) => {
        context.errors = error.response.data.errors
    })
}

export const fetchStatusList = ({ commit }) => {
    return axios.get('/api/progressstatuseslist').then((response) => {
        commit('setStatusList', response.data.data)
    })
}