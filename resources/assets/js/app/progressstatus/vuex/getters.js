export const getStatus = (state) => {
    return state.status
}

export const getState = (state) => {
    return state.selectstate
}

export const getProgressStatusList = (state) => {
    return state.statuslist
}