export const setUsers = (state, data) => {
	state.users = data
}

export const setSelectedUser = (state, data) => {
	state.user = data
}

export const setUsersList = (state, data) => {
	state.userslist = data
}

export const setUsersCenterList = (state, data) => {
	state.userscenterlist = data
}