import { Userdashboard } from '../components'

export default [
    {
        path: '/user-dashboard',
        component: Userdashboard,
        name: 'user-dashboard',
        meta: {
            needsAuth: true
        }
    }
]