import Vue from 'vue'
import Vuex from 'vuex'
import dashboard from '../app/dashboard/vuex'
import auth from '../app/auth/vuex'
import users from '../app/users/vuex'
import costcenters from '../app/costcenters/vuex'
import messages from '../app/messages/vuex'
import tasks from '../app/tasks/vuex'
import progressstatus from '../app/progressstatus/vuex'
import categories from '../app/categories/vuex'
import accesslevels from '../app/accesslevels/vuex'
import priorities from '../app/priorities/vuex'
import userdashboard from '../app/userdashboard/vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        dashboard: dashboard,
        auth: auth,
        users: users,
        costcenters: costcenters,
        messages: messages,
        tasks: tasks,
        progressstatus: progressstatus,
        categories: categories,
        accesslevels: accesslevels,
        priorities: priorities,
        userdashboard: userdashboard
    }
})