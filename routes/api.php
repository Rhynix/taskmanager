<?php

use Illuminate\Http\Request;

Route::post('/auth/login', 'Auth\Authcontroller@login');
Route::post('/auth/logout', 'Auth\Authcontroller@logout');
Route::post('/auth/logout', 'Auth\Authcontroller@logout');
Route::post('/auth/passwordreset', 'Auth\ForgotCredentialsController@resetPassword');

Broadcast::routes(['middleware' => ['jwt.auth']]);
Route::group(['middleware' => 'jwt.auth'], function () {
    Route::get('/auth/user', 'Auth\Authcontroller@user');
    // User
    Route::post('/user/store', 'Users\UsersController@store');
    Route::post('/user/avatar', 'Users\AvatarController@store');
    Route::get('/users', 'Users\UsersController@index');
    Route::put('/changestate/{id}/status', 'Users\UsersController@changestate');
    Route::put('/users/{id}/update','Users\UsersController@update');
    Route::put('/profile/{id}/update','Users\UsersController@updateprofile');
    Route::get('/userslist', 'Users\UsersController@userslist');
    Route::get('/userdepartmentlist', 'Users\UsersController@userdepartmentlist');
    
    // Cost centers
    Route::get('/costcenters', 'Departments\DepartmentsController@index');
    Route::post('/center/create', 'Departments\DepartmentsController@store');
    Route::put('/costcenter/{id}/update', 'Departments\DepartmentsController@update');
    Route::get('/centerlist', 'Departments\DepartmentsController@selectlist');

    // Progress status
    Route::get('/progressstatuses', 'Progressstatuses\ProgressstatusesController@index');
    Route::post('/progressstatus/create', 'Progressstatuses\ProgressstatusesController@store');
    Route::put('/progressstatus/{id}/update', 'Progressstatuses\ProgressstatusesController@update');
    Route::get('/progressstatuseslist', 'Progressstatuses\ProgressstatusesController@proggresslist');

    // Categories
    Route::get('/categories', 'Categories\CategoriesController@index');
    Route::post('/category/create', 'Categories\CategoriesController@store');
    Route::put('/category/{id}/update', 'Categories\CategoriesController@update');
    Route::get('/categorieslist', 'Categories\CategoriesController@categorieslist');

    // Priorities
    Route::get('/accesslevels', 'Accesslevel\AccesslevelController@index');
    Route::post('/accesslevel/create', 'Accesslevel\AccesslevelController@store');
    Route::put('/accesslevel/{id}/update', 'Accesslevel\AccesslevelController@update');
    Route::get('/accesslevelslist', 'Accesslevel\AccesslevelController@accesslevellist');

    // Priorities
    Route::get('/priorities', 'Priorities\PrioritiesController@index');
    Route::post('/priority/create', 'Priorities\PrioritiesController@store');
    Route::put('/priority/{id}/update', 'Priorities\PrioritiesController@update');
    Route::get('/prioritieslist', 'Priorities\PrioritiesController@prioritieslist');

    // Tasks
    Route::get('/tasks', 'Tasks\TasksController@index');
    Route::post('/task/create', 'Tasks\TasksController@store');
    Route::put('/task/{id}/update', 'Tasks\TasksController@update');
    Route::get('/mytasks', 'Tasks\TasksController@mytasks');
    Route::post('/newtaskuser/create', 'Tasks\TasksController@newtaskuser');
    
    Route::post('/taskdatesadjustments/create', 'Taskdatesadjustments\TaskdatesadjustmentsController@store');
    Route::post('/newcomment/create', 'Tasks\TasksController@newcomment');
});
