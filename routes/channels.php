<?php
Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
}, ['guards' => ['api']]);

// Broadcast::channel('App.User.{id}', function ($user, $id) {
//     Auth::check();
//     return User::find($id);
// });

// Broadcast::routes(['middleware' => 'jwt.auth']);

// Broadcast::channel('callbacks', function ($user) {
//     return [
//         'id'        => $user->id,
//         'full_name' => $user->full_name,
//         'avatar' 	=> $user->avatar,
//         'center' 	=> $user->costcenter,
//     ];
// });

// Broadcast::channel('App.User.*', function ($user) {
// 	return (int) $user->id;
// });

// Broadcast::channel('App.User.*', function ($user, $id) {
//     return (int) $user->id === (int) $id;
// }); 

// Broadcast::channel('App.User', function ($user) {
//     return $user;
// });



// Broadcast::channel('App.User.{id}', function ($user) {
//     return true;
// });