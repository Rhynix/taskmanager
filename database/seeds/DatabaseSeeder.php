<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CostcenterTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(progresststusTableSeeder::class);
        $this->call(AccesslevelTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(PrioritiesTableSeeder::class);
    }
}
