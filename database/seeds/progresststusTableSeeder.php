<?php

use Illuminate\Database\Seeder;
use App\Models\Progressstatus\Progressstatus;

class progresststusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $states = [
            [
                'name' => 'On-going',
                'description' => 'The task is being worked on.',
                'user_id' => 2,
            ],
            [
                'name' => 'Completed',
                'description' => 'The task has been completed.',
                'user_id' => 1,
            ],
            [
                'name' => 'Pending',
                'description' => 'The task has not yet been started.',
                'user_id' => 2,
            ]
        ];

        foreach ($states as $state){
            Progressstatus::create($state);
        }
    }
}
