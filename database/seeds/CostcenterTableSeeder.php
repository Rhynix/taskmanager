<?php

use Illuminate\Database\Seeder;
use App\Models\Departments\Departments;

class CostcenterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $department = [
            [
                'code' => 254,
                'name' => 'User interface',
                'email' => 'jhnwanyoike@gmail.com',
                'user_id' => 1
            ],
            [
                'code' => 999,
                'name' => 'Backend',
                'email' => 'john.wanyoike@tdfgroup.co.ke',
                'user_id' => 1
            ]
        ];

        foreach ($department as $center){
            Departments::create($center);
        }
    }
}
