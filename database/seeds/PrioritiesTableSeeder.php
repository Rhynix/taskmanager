<?php

use Illuminate\Database\Seeder;
use App\Models\Priority\Priority;

class PrioritiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $priority = [
            [
                'name' => 'High',
                'description' => 'Urgently needed without moving due date',
                'user_id' => 2,
            ],
            [
                'name' => 'Medium',
                'description' => 'Flexibility of delivery in 2 weeks',
                'user_id' => 1,
            ],
            [
                'name' => 'Low',
                'description' => 'Open time for delivery',
                'user_id' => 1,
            ]
        ];

        foreach ($priority as $pri){
            Priority::create($pri);
        }
    }
}
