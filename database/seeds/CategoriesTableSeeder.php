<?php

use Illuminate\Database\Seeder;
use App\Models\Categories\Categories;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $access = [
            [
                'name' => 'Default',
                'description' => 'Category that carries all tasks that are not categorised',
                'user_id' => 1,
            ],
            [
                'name' => 'Authentication',
                'description' => 'Where security of the system is defined',
                'user_id' => 2,
            ],
            [
                'name' => 'User management',
                'description' => 'Sprint to manage user roles in the system',
                'user_id' => 1,
            ]
        ];

        foreach ($access as $acc){
            Categories::create($acc);
        }
    }
}
