<?php

use Illuminate\Database\Seeder;
use App\Models\Accesslevel\Accesslevel;

class AccesslevelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $access = [
            [
                'name' => 'Private',
                'description' => 'Only accessed by asigned members only',
                'user_id' => 2,
            ],
            [
                'name' => 'Public',
                'description' => 'Can be accessed by anyone',
                'user_id' => 1,
            ]
        ];

        foreach ($access as $acc){
            Accesslevel::create($acc);
        }
    }
}
