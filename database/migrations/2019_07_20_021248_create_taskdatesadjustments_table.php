<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskdatesadjustmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('taskdatesadjustments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tasks_id')->unsigned()->index();
            $table->date('date_from');
            $table->date('date_to')->nullable();
            $table->integer('user_id')->unsigned()->index()->nullable();
            $table->integer('department_id')->unsigned()->index()->nullable();
            $table->string('reason', 200);
            $table->timestamps();

            $table->foreign('tasks_id')->references('id')->on('tasks');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('department_id')->references('code')->on('departments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('taskdatesadjustments');
    }
}
