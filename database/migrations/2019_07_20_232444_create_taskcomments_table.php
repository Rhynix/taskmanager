<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskcommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('taskcomments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tasks_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
            $table->string('comment', 200);
            $table->timestamps();

            $table->foreign('tasks_id')->references('id')->on('tasks');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('taskcomments');
    }
}
