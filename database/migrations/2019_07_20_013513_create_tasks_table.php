<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('task_uid', 200);
            $table->integer('user_id')->unsigned()->index();
            $table->date('due_date')->nullable();
            $table->integer('progress_status')->unsigned()->index()->nullable();
            $table->string('title', 20);
            $table->string('description', 200);
            $table->integer('access_level')->unsigned()->index()->comment('private or public');
            $table->integer('category')->unsigned()->index()->nullable();
            $table->integer('priority')->unsigned()->index();
            $table->integer('department')->unsigned()->index()->nullable();
            $table->integer('assigened_user')->unsigned()->index()->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('assigened_user')->references('id')->on('users');
            $table->foreign('progress_status')->references('id')->on('progressstatuses');
            $table->foreign('access_level')->references('id')->on('accesslevels');
            $table->foreign('category')->references('id')->on('categories');
            $table->foreign('priority')->references('id')->on('priorities');
            $table->foreign('department')->references('code')->on('departments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
