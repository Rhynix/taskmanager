<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskusersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('taskusers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tasks_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index()->nullable();
            $table->integer('department_id')->unsigned()->index()->nullable();
            $table->string('comment', 200);
            $table->timestamps();

            $table->foreign('tasks_id')->references('id')->on('tasks');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('department_id')->references('code')->on('departments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('taskusers');
    }
}
